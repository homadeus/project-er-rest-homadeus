all: er-rest-homadeus 

ifndef TARGET
TARGET=avr-homadeus
endif

avrdude:
	sudo make er-rest-homadeus.$(TARGET).u AVRDUDE_PROGRAMMER="-e -c usbasp" #  AVRDUDE_OPTIONS="-V" AVRDUDE_PORT=""

CONTIKI=../../os/contiki
CFLAGS += -DPROJECT_CONF_H=\"project-conf.h\"

# variable for this Makefile
# configure CoAP implementation (3|7|12|13) (er-coap-07 also supports CoAP draft 08)
WITH_COAP=13


# variable for Makefile.include
WITH_UIP6=1
# for some platforms
UIP_CONF_IPV6=1
# IPv6 make config disappeared completely
CFLAGS += -DUIP_CONF_IPV6=1


UIP_CONF_IPV6_RPL=1 #this is from homadeus makefile

# radio configuration
#CFLAGS += -DRF_CHANNEL=20
#CFLAGS += -DIEEE802154_CONF_PANID=0xCAFE

CFLAGS += -DUIP_CONF_IPV6_RPL=1


# linker optimizations
SMALL=1

# REST framework, requires WITH_COAP
ifeq ($(WITH_COAP), 13)
${info INFO: compiling with CoAP-13}
CFLAGS += -DWITH_COAP=13
CFLAGS += -DREST=coap_rest_implementation
CFLAGS += -DUIP_CONF_TCP=0

APPS += er-coap-13

endif

APPS += erbium

LDFLAGS += -Wl,-u,vfprintf -lprintf_flt -lm


MY_APPS += bootloader_control spi_driver tiki_utils twi_driver twi_78M6610 spi_flash fridge_process

APPS += $(MY_APPS) hmd_relay_process
APPDIRS += $(join $(addprefix ../../apps/,$(MY_APPS)), )

# optional rules to get assembly
#CUSTOM_RULE_C_TO_OBJECTDIR_O = 1
#CUSTOM_RULE_S_TO_OBJECTDIR_O = 1

include $(CONTIKI)/Makefile.include

# optional rules to get assembly
#$(OBJECTDIR)/%.o: asmdir/%.S
#	$(CC) $(CFLAGS) -MMD -c $< -o $@
#	@$(FINALIZE_DEPENDENCY)
#
#asmdir/%.S: %.c
#	$(CC) $(CFLAGS) -MMD -S $< -o $@

# border router rules
$(CONTIKI)/tools/tunslip6:	$(CONTIKI)/tools/tunslip6.c
	(cd $(CONTIKI)/tools && $(MAKE) tunslip6)

connect-router:	$(CONTIKI)/tools/tunslip6
	sudo $(CONTIKI)/tools/tunslip6 aaaa::1/64

