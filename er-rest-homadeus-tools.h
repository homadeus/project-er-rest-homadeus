/*
 * er-rest-homadeus-tools.h
 *
 *  Created on: Aug 14, 2013
 *      Author: renzo
 */

#ifndef ER_REST_HOMADEUS_TOOLS_H_
#define ER_REST_HOMADEUS_TOOLS_H_

//#include <homadeus/utils/debug.h>

#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

#include <twi/twi.h> // for twi_writeTo
#include <homadeus/devices/78M6610.h> // for MAXIM_78M6610_TWI_ADDR
#include <homadeus/utils/hex.h> // for bin2hex



#endif /* ER_REST_HOMADEUS_TOOLS_H_ */
