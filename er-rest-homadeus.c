/**
 * \file
 *      Homadeus platform CoAP-Server uding Erbium (Er) REST Engine
 * \author
 *      XX XX <xx@xx.xx>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "contiki.h"
#include "contiki-net.h"

#include "erbium.h"

#if WITH_COAP == 13
#include "er-coap-13.h"
#else
#warning "Erbium without CoAP-specifc functionality"
#endif /* CoAP-specific example */


#include <twi/twi.h>
#include <homadeus/utils/hex.h>
#include <homadeus/processes/relay.h>
#include <homadeus/devices/78M6610.h>
#include <homadeus/processes/fridge_process.h>

#include <homadeus/boards/board.h>
#include <homadeus/bootloader/bootloader.h>

#include <avr/wdt.h>





/* Define which resources to include to meet memory constraints. */
//#define REST_RES_HELLO 0


#if defined (PLATFORM_HAS_BUTTON)
#include "dev/button-sensor.h"
#endif
#if defined (PLATFORM_HAS_LEDS)
#include "dev/leds.h"
#endif
/*
#if defined (PLATFORM_HAS_LIGHT)
#include "dev/light-sensor.h"
#endif
#if defined (PLATFORM_HAS_BATTERY)
#include "dev/battery-sensor.h"
#endif
#if defined (PLATFORM_HAS_SHT11)
#include "dev/sht11-sensor.h"
#endif
#if defined (PLATFORM_HAS_RADIO)
#include "dev/radio-sensor.h"
#endif
*/

#define DEBUG 0
#include <homadeus/utils/debug.h>

/* Its defined on  homadeus/utils/debug.h
#if DEBUG
#define PRINTF(...) printf(__VA_ARGS__)
#define PRINT6ADDR(addr) PRINTF("[%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x]", ((uint8_t *)addr)[0], ((uint8_t *)addr)[1], ((uint8_t *)addr)[2], ((uint8_t *)addr)[3], ((uint8_t *)addr)[4], ((uint8_t *)addr)[5], ((uint8_t *)addr)[6], ((uint8_t *)addr)[7], ((uint8_t *)addr)[8], ((uint8_t *)addr)[9], ((uint8_t *)addr)[10], ((uint8_t *)addr)[11], ((uint8_t *)addr)[12], ((uint8_t *)addr)[13], ((uint8_t *)addr)[14], ((uint8_t *)addr)[15])
#define PRINTLLADDR(lladdr) PRINTF("[%02x:%02x:%02x:%02x:%02x:%02x]",(lladdr)->addr[0], (lladdr)->addr[1], (lladdr)->addr[2], (lladdr)->addr[3],(lladdr)->addr[4], (lladdr)->addr[5])
#else
#define PRINTF(...)
#define PRINT6ADDR(addr)
#define PRINTLLADDR(addr)
#endif
*/

//#include "er-rest-homadeus-tools.h"


char temp[256];

#define ADD_VALUE(counter, value, length, last) \
  do { counter += add_value_to_string(temp + counter, MAXIM_78M6610_##value, #value, length, sizeof(temp) - counter - 1, last); } while(0)


int add_value_to_string(char* str, uint8_t reg, char* value_name, uint8_t len, int max, uint8_t last) {
    char dest[len * 2 + 1];
    PRINTF("Reading: %s : ", value_name);
    uint8_t data[len];
    int rc = twi_writeTo(MAXIM_78M6610_TWI_ADDR, &reg, 1, 1);
    if (rc) {
      PRINTF(" [error writing to 78M6610: %d] ", rc);
    }
    rc = twi_readFrom(MAXIM_78M6610_TWI_ADDR, data, len);
    bin2hex(dest, data, len);
    dest[len*2] = 0;
    PRINTF("%s (%d of %d read)\n", dest, rc, len);
    if (last)
      return snprintf(str, max, "\"%s\":\"%s\"", value_name, dest);
    else
      return snprintf(str, max, "\"%s\":\"%s\",", value_name, dest);
}





/* Resource Definition Read From:
 *
 * 	* http://www.ipso-alliance.org/wp-content/media/draft-ipso-app-framework-04.pdf -
 * 	* http://tools.ietf.org/html/rfc6690 - Constrained RESTful Environments (CoRE) Link Format
 * 	* http://tools.ietf.org/html/draft-ietf-core-interfaces-00#section-5 (CoRE Interfaces)
 */

#define COAP_URI_VOLTAGE	"volt"
#define COAP_URI_CURRENT	"current"
#define COAP_URI_POWER 		"pwr"

#define COAP_URI_POWER_WATT	COAP_URI_POWER"/w"

#define COAP_URI_TB 		 		"tb"
#define COAP_URI_TB_HARMONICS		COAP_URI_TB"/harmonics"
#define COAP_URI_TB_SAMPLING_RATE 	COAP_URI_TB"/sampling"

//FRIDGE_PROCESS
#define COAP_URI_FRIDGE_PROCESS "fridge_process"
#define COAP_URI_FP_MEAN  	COAP_URI_FRIDGE_PROCESS"/mean"
#define COAP_URI_FP_UP_TIME  	COAP_URI_FRIDGE_PROCESS"/up_time"
#define COAP_URI_FP_TOTAL_TIME  COAP_URI_FRIDGE_PROCESS"/total_time"
/******************************************************************************/
/*** MAXIM REGISTERS BULK (Debug)
 *
 */

RESOURCE(maxim_scales, METHOD_GET, COAP_URI_TB "/" COAP_URI_POWER "/maxim_scales", "title=\"Scale Registers\";rt=\"tb.maxim.pwr.scales\";ct=\"0\";if=\"core.rp\"");
void
maxim_scales_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{

  int count = 0;



  count += snprintf(temp, sizeof(temp) - count - 1, "{");

  ADD_VALUE(count, I_SCALE	, 3, 0); // "Current scaling register"
  ADD_VALUE(count, V_SCALE	, 3, 0); // "Voltage scaling register"
  ADD_VALUE(count, P_SCALE	, 3, 0); // "Power scaling register"
  ADD_VALUE(count, F_SCALE	, 3, 0); // "Frequency scaling register"
  ADD_VALUE(count, T_SCALE	, 3, 0); // "Temperature scaling register"
  ADD_VALUE(count, PF_SCALE , 3, 1); // "Power Factor scaling register"

  count += snprintf(temp + count, sizeof(temp) - count - 1, "}");

  wdt_disable();

  REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  //REST.set_header_etag(response, (uint8_t *) &length, 1);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));


}



RESOURCE(maxim_sensors, METHOD_GET, COAP_URI_TB "/" COAP_URI_POWER "/maxim_sensors", "title=\"Sensor Values\";rt=\"tb.maxim.pwr.sen\";ct=\"0\";if=\"core.rp\"");
void
maxim_sensors_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{

  int count = 0;
  count += snprintf(temp, sizeof(temp) - count - 1, "{");
  ADD_VALUE(count, FREQUENCY, 3, 0); // works
  ADD_VALUE(count, V_RMS , 3, 0);
  ADD_VALUE(count, I_RMS , 3, 0);
  ADD_VALUE(count, P, 3, 0);
  ADD_VALUE(count, P_AVG, 3, 0);
  ADD_VALUE(count, TEMPERATURE, 3, 1);
  count += snprintf(temp + count, sizeof(temp) - count - 1, "}");

  wdt_disable();
  /*
  maxim_78M6610_debug_version(temp);
  TEMPERATURE_type t = maxim_78M6610_read_temp(ttemp);
  sprintf(temp,"%s/%s/%g: Hello World!\n", temp, ttemp, t);
  */

  REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  //REST.set_header_etag(response, (uint8_t *) &length, 1);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));


}



RESOURCE(maxim_sensors_unscaled, METHOD_GET, COAP_URI_TB "/" COAP_URI_POWER "/maxim_sensors_unscaled", "title=\"Sensor Values UNSCALED\";rt=\"tb.maxim.pwr.sen.unscaled\";ct=\"0\";if=\"core.rp\"");
void
maxim_sensors_unscaled_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{

  int count = 0;

  count += snprintf(temp, sizeof(temp) - count - 1, "{");
  ADD_VALUE(count, FREQUENCY_UNSCALED, 3, 0);
  ADD_VALUE(count, V_RMS_UNSCALED , 3, 0);
  ADD_VALUE(count, I_RMS_UNSCALED , 3, 0);
  ADD_VALUE(count, P_RMS_UNSCALED, 3, 0);

  ADD_VALUE(count, VA_REACTIVE_UNSCALED, 3, 0);
  ADD_VALUE(count, VA_UNSCALED, 3, 0);
  ADD_VALUE(count, POWER_FACTOR_UNSCALED, 3, 1);

  count += snprintf(temp + count, sizeof(temp) - count - 1, "}");

  wdt_disable();

  REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  //REST.set_header_etag(response, (uint8_t *) &length, 1);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));


}


/******************************************************************************/
/*** MAXIM HARMONICS MEASURES
 *
 */

#define HARMONIC_SELECTOR_MAX_HARMONIC 10 // Max harmonic allowed to measure

RESOURCE(maxim_harmonic_selector, METHOD_GET | METHOD_PUT , COAP_URI_TB_HARMONICS"/harmonic_selector", "title=\"Harmonic Selector\";rt=\"tb.maxim.harm.sel\";ct=\"0\";if=\"core.p\"");
void
maxim_harmonic_selector_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
	//coap_packet_t *const coap_req = (coap_packet_t *) request;
	uint8_t method = REST.get_method_type(request);

	if (method & METHOD_GET)
	{
		uint32_t value = maxim_78M6610_get_register_uint24(MAXIM_78M6610_HARM);
		snprintf(temp, sizeof(temp), "%"PRIu32"", value);
		REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
	}
	else if (method & METHOD_PUT)
	{
		REST.set_response_payload(response, NULL, 0);
	    uint8_t *incoming = NULL;
	    size_t len = 0;
		len = REST.get_request_payload(request, (const uint8_t **) &incoming);


		// Parse the string as unsigned long and store it on an array of uint8_t
		char **tailptr = NULL;
		unsigned long parsed_long = strtoul((char *)incoming, tailptr, 10);
		uint8_t parsed_bytes[sizeof(unsigned long)];
		memcpy(parsed_bytes, &parsed_long, sizeof(parsed_bytes));


		int success = 0;

		if ( (0 < parsed_long) && (parsed_long < HARMONIC_SELECTOR_MAX_HARMONIC ) ) success = 1;

		if (!success) {
			REST.set_response_status(response, REST.status.BAD_REQUEST);
			char str[40];
			len = sprintf(str, "len:%d . incoming:%s", len, (char*)incoming);
			REST.set_response_payload(response, str, len);
			return;
		}

		// Set harmonic_selector to the value of parsed_long
		uint8_t data_to_write[3] = { parsed_bytes[2] , parsed_bytes[1] , parsed_bytes[0]}; // Endianness is different for the I2C protocol
		maxim_78M6610_write_to_register(MAXIM_78M6610_HARM, data_to_write);


		REST.set_response_status(response, REST.status.CHANGED);
		return;

	}
	else {
		REST.set_response_status(response, REST.status.BAD_REQUEST);
		const char *error_msg = "BadREQ";
		REST.set_response_payload(response, error_msg, strlen(error_msg));
		return;
	}

}

//

/*
 *  Doc from MAXIM: Datasheet
 *  Accumulation Interval
		The accumulation interval is configurable by the user through the ACCUM register. The ACCUM register
		contains an unsigned integer values representing the accumulation interval (time) expressed in number of
		high-rate samples.

		The accumulation interval can also be locked to the incoming line voltage cycles. The LINELOCK bit
		in the command register allows the accumulation interval to be determined by the ACCUM register or
		to be locked to the line cycle.

		Once locked to the line cycle the accumulation interval will end after the first low-to-high zero
		crossing of the Reference AC Voltage (see Zero-Crossing Detection) input occurs once the Minimum
		Accumulation time has elapsed. The Actual Accumulation Interval will span an integer number of line
		cycles. When LINELOCK is not set, the Accumulation Interval will equal the value set by the ACCUM
		register.

		The effective sample rate for each input of the 78M6610+PSU is 4KS/s. The DIVISOR register
		reports the actual number of samples within any given accumulation interval.
 *
 * */

// ACCUM , UINT24 , "Accumulation Interval for calculation (RMS, etc.). [Default = 400]" // GET and SET

#define MAX_UINT24 16777215

RESOURCE(maxim_accumulation_interval, METHOD_GET | METHOD_PUT, COAP_URI_TB_SAMPLING_RATE"/accumulation_interval", "title=\"Accumulation Interval for calculation (RMS, etc.). [Default = 400]\";rt=\"ucum.{Number of high-rate samples -4KSamples/s-}\";ct=\"0\";if=\"core.p\"");
void
maxim_accumulation_interval_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{

	coap_packet_t *const coap_req = (coap_packet_t *) request;

	uint8_t method = REST.get_method_type(request);

	if (method & METHOD_GET)
	{
		uint32_t value = maxim_78M6610_get_register_uint24(MAXIM_78M6610_ACCUM);
		snprintf(temp, sizeof(temp), "%"PRIu32"", value);
		REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
	}
	else if (method & METHOD_PUT)
	{
		REST.set_response_payload(response, NULL, 0);
		    uint8_t *incoming = NULL;
		    size_t len = 0;
			len = REST.get_request_payload(request, (const uint8_t **) &incoming);


			// Parse the string as unsigned long and store it on an array of uint8_t
			char **tailptr = NULL;
			unsigned long parsed_long = strtoul((char *)incoming, tailptr, 10);
			uint8_t parsed_bytes[sizeof(unsigned long)];
			memcpy(parsed_bytes, &parsed_long, sizeof(parsed_bytes));


			int success = 0;

			if ( (0 < parsed_long) && (parsed_long <= MAX_UINT24 ) ) success = 1;

			if (!success) {
				REST.set_response_status(response, REST.status.BAD_REQUEST);
				char str[40];
				len = sprintf(str, "len:%d . incoming:%s", len, (char*)incoming);
				REST.set_response_payload(response, str, len);
				return;
			}

			// Set harmonic_selector to the value of parsed_long
			uint8_t data_to_write[3] = { parsed_bytes[2] , parsed_bytes[1] , parsed_bytes[0]}; // Endianness is different for the I2C protocol
			maxim_78M6610_write_to_register(MAXIM_78M6610_ACCUM, data_to_write);


			REST.set_response_status(response, REST.status.CHANGED);
			return;


	}
	else {
		REST.set_response_status(response, REST.status.BAD_REQUEST);
		const char *error_msg = "BadREQ";
		REST.set_response_payload(response, error_msg, strlen(error_msg));
		return;
	}
}


// DIVISOR , UINT24 , "Actual Accumulation interval for low rate results" // GET
RESOURCE(maxim_divisor, METHOD_GET, COAP_URI_TB_SAMPLING_RATE"/divisor", "title=\"Actual Accumulation interval for low rate results\";rt=\"ucum.{Number of samples within any given accumulation interval}\";ct=\"0\";if=\"core.rp\"");
void
maxim_divisor_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  //wdt_disable();
  uint32_t value = maxim_78M6610_get_register_uint24(MAXIM_78M6610_DIVISOR);
  snprintf(temp, sizeof(temp), "%"PRIu32"", value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

/******************************************************************************/
/*** VOLTAGE MEASURES
 *
 *
 * Voltage Measures:
 *
 * Vrms USI24 RMS Voltage (LSB weight determined by Vscale)
 * Vfund USI24 RMS Voltage (Fundamental)
 * Vharm USI24 RMS Voltage (Harmonic)
 * VLow USI24 Lowest RMS Voltage Recorded Since Reset (LSB determined by  Vscale)
 * VHigh USI24 Highest RMS Voltage Recorded Since Reset (LSB weight determined by Vscale)
 * V_PEAK , USI24  , "Highest Voltage in last accumulation Interval (LSB weight determined by Vscale)"
 * Vrms1 UINT24 Unscaled RMS Voltage
 *
 *
 */


RESOURCE(maxim_voltage, METHOD_GET, COAP_URI_VOLTAGE, "title=\"RMS Voltage\";rt=\"ucum.V\";ct=\"0\";if=\"core.s\"");
void
maxim_voltage_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  //wdt_disable();
  double voltage_rms = MAXIM_78M6610_SCALING_RESOLUTION_VOLTAGE_VOLTS * maxim_78M6610_get_voltage_rms();
  snprintf(temp , sizeof(temp), "%0.3f", voltage_rms);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

//V_PEAK , USI24  , "Highest Voltage in last accumulation Interval (LSB weight determined by Vscale)"
RESOURCE(maxim_voltage_peak, METHOD_GET, COAP_URI_VOLTAGE"/peak", "title=\"Highest Voltage in last accumulation Interval\";rt=\"ucum.V\";ct=\"0\";if=\"core.s\"");
void
maxim_voltage_peak_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_VOLTAGE_VOLTS * maxim_78M6610_get_register_uint24(MAXIM_78M6610_V_PEAK);
  snprintf(temp , sizeof(temp) , "%0.3f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


RESOURCE(maxim_voltage_fundamental, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_VOLTAGE"/fund", "title=\"RMS Voltage (Fundamental)\";rt=\"ucum.V\";ct=\"0\";if=\"core.s\"");
void
maxim_voltage_fundamental_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_VOLTAGE_VOLTS * maxim_78M6610_get_register_uint24(MAXIM_78M6610_V_FUND);
  snprintf(temp , sizeof(temp) , "%0.3f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

//Vharm USI24 RMS Voltage (Harmonic)
RESOURCE(maxim_voltage_harmonic, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_VOLTAGE"/harm", "title=\"RMS Voltage (Harmonic)\";rt=\"ucum.V\";ct=\"0\";if=\"core.s\"");
void
maxim_voltage_harmonic_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_VOLTAGE_VOLTS * maxim_78M6610_get_register_uint24(MAXIM_78M6610_V_HARMONIC);
  snprintf(temp , sizeof(temp) , "%0.3f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

//VLow USI24 Lowest RMS Voltage Recorded Since Reset (LSB determined by  Vscale)
RESOURCE(maxim_voltage_low, METHOD_GET, COAP_URI_VOLTAGE"/low", "title=\"Lowest RMS Voltage Recorded Since Reset\";rt=\"ucum.V\";ct=\"0\";if=\"core.s\"");
void
maxim_voltage_low_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_VOLTAGE_VOLTS * maxim_78M6610_get_register_uint24(MAXIM_78M6610_V_LOW);
  snprintf(temp , sizeof(temp) , "%0.3f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

//VHigh USI24 Highest RMS Voltage Recorded Since Reset (LSB weight determined by Vscale)
RESOURCE(maxim_voltage_high, METHOD_GET, COAP_URI_VOLTAGE"/high", "title=\"Highest RMS Voltage Recorded Since Reset\";rt=\"ucum.V\";ct=\"0\";if=\"core.s\"");
void
maxim_voltage_high_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_VOLTAGE_VOLTS * maxim_78M6610_get_register_uint24(MAXIM_78M6610_V_HIGH);
  snprintf(temp , sizeof(temp) , "%0.3f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


/******************************************************************************/
/*** CURRENT MEASURES
 *
 */

PERIODIC_RESOURCE(maxim_current, METHOD_GET, COAP_URI_CURRENT, "title=\"RMS Current\";rt=\"ucum.mA\";ct=\"0\";if=\"core.s\";obs", 3*CLOCK_SECOND );
void
maxim_current_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_CURRENT_AMPERE_MILI * maxim_78M6610_get_register_uint24(MAXIM_78M6610_I_RMS);
  snprintf(temp , sizeof(temp), "%0.7f" , scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

/*
 * Additionally, a handler function named [resource name]_handler must be implemented for each PERIODIC_RESOURCE.
 * It will be called by the REST manager process with the defined period.
 */
void
maxim_current_periodic_handler(resource_t *r)
{
  static uint16_t obs_counter = 0;
  ++obs_counter;

  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_CURRENT_AMPERE_MILI * maxim_78M6610_get_register_uint24(MAXIM_78M6610_I_RMS);
  snprintf(temp , sizeof(temp), "%0.7f" , scaled_value);

  /* Build notification. */
  coap_packet_t notification[1]; /* This way the packet can be treated as pointer as usual. */
  coap_init_message(notification, COAP_TYPE_NON, REST.status.OK, 0 );
  coap_set_payload(notification, (uint8_t*)temp , strlen(temp));

  /* Notify the registered observers with the given message type, observe option, and payload. */
  REST.notify_subscribers(r, obs_counter, notification);
}

//I_PEAK , USI24  , "Highest Current in last accumulation Interval (LSB determined by Iscale)"
RESOURCE(maxim_current_peak, METHOD_GET, COAP_URI_CURRENT"/peak", "title=\"Highest Current in last accumulation Interval\";rt=\"ucum.mA\";ct=\"0\";if=\"core.s\"");
void
maxim_current_peak_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_CURRENT_AMPERE_MILI * maxim_78M6610_get_register_uint24(MAXIM_78M6610_I_PEAK);
  snprintf(temp , sizeof(temp), "%0.7f" , scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


//
RESOURCE(maxim_current_fundamental, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_CURRENT"/fund", "title=\"RMS Current (Fundamental)\";rt=\"ucum.mA\";ct=\"0\";if=\"core.s\"");
void
maxim_current_fundamental_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_CURRENT_AMPERE_MILI * maxim_78M6610_get_register_uint24(MAXIM_78M6610_I_FUND);
  snprintf(temp , sizeof(temp), "%0.7f" , scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

RESOURCE(maxim_current_harmonic, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_CURRENT"/harm", "title=\"RMS Current (Harmonic)\";rt=\"ucum.mA\";ct=\"0\";if=\"core.s\"");
void
maxim_current_harmonic_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_CURRENT_AMPERE_MILI * maxim_78M6610_get_register_uint24(MAXIM_78M6610_I_HARMONIC);
  snprintf(temp , sizeof(temp), "%0.7f" , scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


RESOURCE(maxim_current_low, METHOD_GET, COAP_URI_CURRENT"/low", "title=\"Lowest RMS Current Recorded Since Reset\";rt=\"ucum.mA\";ct=\"0\";if=\"core.s\"");
void
maxim_current_low_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_CURRENT_AMPERE_MILI * maxim_78M6610_get_register_uint24(MAXIM_78M6610_I_LOW);
  snprintf(temp , sizeof(temp), "%0.7f" , scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

RESOURCE(maxim_current_high, METHOD_GET, COAP_URI_CURRENT"/high", "title=\"Highest RMS Current Recorded Since Reset\";rt=\"ucum.mA\";ct=\"0\";if=\"core.s\"");
void
maxim_current_high_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_CURRENT_AMPERE_MILI * maxim_78M6610_get_register_uint24(MAXIM_78M6610_I_HIGH);
  snprintf(temp , sizeof(temp), "%0.7f" , scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}



/******************************************************************************/
/*** POWER MEASURES
 *
 *
 * Watt SSI24 Active Power (LSB weight determined by Pscale)
 * PAverage SSI24 Active Power Averaged over 30s Window (LSB determined by Pscale)
 *
 * VA SSI24 Apparent Power (LSB weight determined by Pscale)
 * VAR SSI24 Reactive Power (LSB weight determined by Pscale)
 *
 * POWER_FACTOR SSI24  "Power Factor (LSB weight determined by PFscale) // P/S
 *
 *
 * Pfund SSI24 Active Power (Fundamental)
 * Qfund SSI24 Reactive Power (Fundamental) (LSB weight determined by
 * VAfund SSI24 Apparent Power (Fundamental) (LSB weight deter
 *
 * Pharm SSI24 Active Power (Harmonic) (LSB weight determined by Pscale)
 * Qharm SSI24 Reactive Power (Harmonic) (LSB weight determined by Pscale)
 * VAharm SSI24 Apparent Power (Harmonic) (LSB weight determined by Pscale)
 *
 *
 * About: http://www.allaboutcircuits.com/vol_2/chpt_11/2.html
 *
 */


PERIODIC_RESOURCE(maxim_power, METHOD_GET, COAP_URI_POWER_WATT, "title=\"Active Power (P)\";rt=\"ipso.pwr.w\";ct=\"0\";if=\"core.s\";obs", 3*CLOCK_SECOND );
void
maxim_power_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  //wdt_disable();
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_P);
  //if(scaled_value<0) scaled_value = 0; // Quick FIX to show only positive values.
  snprintf(temp , sizeof(temp), "%0.7f", scaled_value);
  REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

void
maxim_power_periodic_handler(resource_t *r)
{
  static uint16_t obs_counter = 0;
  ++obs_counter;

  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_P);
  //if(scaled_value<0) scaled_value = 0; // Quick FIX to show only positive values.
  snprintf(temp , sizeof(temp), "%0.7f", scaled_value);

  /* Build notification. */
  coap_packet_t notification[1]; /* This way the packet can be treated as pointer as usual. */
  coap_init_message(notification, COAP_TYPE_NON, REST.status.OK, 0 );
  coap_set_payload(notification, (uint8_t*)temp , strlen(temp));

  /* Notify the registered observers with the given message type, observe option, and payload. */
  REST.notify_subscribers(r, obs_counter, notification);
}


RESOURCE(maxim_power_avg, METHOD_GET, COAP_URI_POWER_WATT"/active_average", "title=\"Active Power (P) Averaged over 30s Window\";rt=\"ucum.W\";ct=\"0\";if=\"core.rp\"");
void
maxim_power_avg_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  //wdt_disable();
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_P_AVG);
  //if(scaled_value<0) scaled_value = 0; // Quick FIX to show only positive values.
  snprintf(temp , sizeof(temp), "%0.7f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


// VA SSI24 Apparent Power (LSB weight determined by Pscale)
RESOURCE(maxim_power_apparent, METHOD_GET, COAP_URI_POWER_WATT"/apparent", "title=\"Apparent Power (S)\";rt=\"ucum.W{Volt-Ampere (VA)}\";ct=\"0\";if=\"core.s\"");
void
maxim_power_apparent_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_VA);
  snprintf(temp , sizeof(temp), "%0.7f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

// VAR SSI24 Reactive Power (LSB weight determined by Pscale)
RESOURCE(maxim_power_reactive, METHOD_GET, COAP_URI_POWER_WATT"/reactive", "title=\"Reactive Power (Q)\";rt=\"ucum.W{Volt-Ampere Reactive (VAR)}\";ct=\"0\";if=\"core.s\"");
void
maxim_power_reactive_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_VA_REACTIVE);
  snprintf(temp , sizeof(temp), "%0.7f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

// POWER_FACTOR SSI24   "Power Factor (LSB weight determined by PFscale)
// PF_SCALE     UINT24  "Power Factor scaling register"
RESOURCE(maxim_power_factor, METHOD_GET, COAP_URI_POWER_WATT"/power_factor", "title=\"Power Factor (P/S)\";rt=\"ucum.{Power Factor}\";ct=\"0\";if=\"core.s\"");
void
maxim_power_factor_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = (double)maxim_78M6610_get_register_int24(MAXIM_78M6610_POWER_FACTOR) / (double) maxim_78M6610_get_register_uint24(MAXIM_78M6610_PF_SCALE);
  snprintf(temp , sizeof(temp), "%0.8f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


// Pfund SSI24 Active Power (Fundamental)
RESOURCE(maxim_power_fundamental_active, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_POWER_WATT"/fund/active", "title=\"Active Power (Fundamental)\";rt=\"ucum.W\";ct=\"0\";if=\"core.s\"");
void
maxim_power_fundamental_active_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_P_FUND);
  snprintf(temp , sizeof(temp) , "%0.7f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

// Qfund SSI24 Reactive Power (Fundamental)
RESOURCE(maxim_power_fundamental_reactive, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_POWER_WATT"/fund/reactive", "title=\"Reactive Power (Fundamental)\";rt=\"ucum.W{VAR}\";ct=\"0\";if=\"core.s\"");
void
maxim_power_fundamental_reactive_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_Q_FUND);
  snprintf(temp , sizeof(temp), "%0.7f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

// VAfund SSI24 Apparent Power (Fundamental) (LSB weight determined by Pscale)
RESOURCE(maxim_power_fundamental_apparent, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_POWER_WATT"/fund/apparent", "title=\"Apparent Power (Fundamental)\";rt=\"ucum.W{VA}\";ct=\"0\";if=\"core.s\"");
void
maxim_power_fundamental_apparent_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_VA_FUND);
  snprintf(temp , sizeof(temp), "%0.7f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

// Pharm SSI24 Active Power (Harmonic) (LSB weight determined by Pscale)
RESOURCE(maxim_power_harmonic_active, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_POWER_WATT"/harm/active", "title=\"Active Power (Harmonic)\";rt=\"ucum.W\";ct=\"0\";if=\"core.s\"");
void
maxim_power_harmonic_active_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_P_HARMONIC);
  snprintf(temp , sizeof(temp) , "%0.7f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

// Qharm SSI24 Reactive Power (Harmonic) (LSB weight determined by Pscale)
RESOURCE(maxim_power_harmonic_reactive, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_POWER_WATT"/harm/reactive", "title=\"Reactive Power (Harmonic)\";rt=\"ucum.W{VAR}\";ct=\"0\";if=\"core.s\"");
void
maxim_power_harmonic_reactive_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_Q_HARMONIC);
  snprintf(temp , sizeof(temp) , "%0.7f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

// VAharm SSI24 Apparent Power (Harmonic) (LSB weight determined by Pscale)
RESOURCE(maxim_power_harmonic_apparent, METHOD_GET, COAP_URI_TB_HARMONICS"/"COAP_URI_POWER_WATT"/harm/apparent", "title=\"Apparent Power (Harmonic)\";rt=\"ucum.W{VA}\";ct=\"0\";if=\"core.s\"");
void
maxim_power_harmonic_apparent_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = MAXIM_78M6610_SCALING_RESOLUTION_POWER_WATTS * maxim_78M6610_get_register_int24(MAXIM_78M6610_VA_HARMONIC);
  snprintf(temp , sizeof(temp) , "%0.7f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}



/******************************************************************************/
/*** Frequency
 *
 */

/*
 * Periodic resource.
 * It takes an additional period parameter, which defines the interval to call [name]_periodic_handler().
 * A default post_handler takes care of subscriptions by managing a list of subscribers to notify.
 */
PERIODIC_RESOURCE(maxim_frequency, METHOD_GET, "frequency", "title=\"Line Frequency\";rt=\"ucum.Hz\";ct=\"0\";if=\"core.s\";obs", 3*CLOCK_SECOND );
void
maxim_frequency_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  //wdt_disable();
  double scaled_value = (double)maxim_78M6610_get_frequency() / (double)maxim_78M6610_get_register_uint24(MAXIM_78M6610_F_SCALE);
  snprintf(temp , sizeof(temp), "%0.3f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


/*
 * Additionally, a handler function named [resource name]_handler must be implemented for each PERIODIC_RESOURCE.
 * It will be called by the REST manager process with the defined period.
 */
void
maxim_frequency_periodic_handler(resource_t *r)
{
  static uint16_t obs_counter = 0;
  //static char content[11];
  ++obs_counter;

  double scaled_value = (double)maxim_78M6610_get_frequency() / (double)maxim_78M6610_get_register_uint24(MAXIM_78M6610_F_SCALE);
  snprintf(temp , sizeof(temp), "%0.3f", scaled_value);

  //PRINTF("TICK %u for /%s\n", obs_counter, r->url);

  /* Build notification. */
  coap_packet_t notification[1]; /* This way the packet can be treated as pointer as usual. */
  coap_init_message(notification, COAP_TYPE_NON, REST.status.OK, 0 );
  coap_set_payload(notification, (uint8_t*)temp , strlen(temp));

  /* Notify the registered observers with the given message type, observe option, and payload. */
  REST.notify_subscribers(r, obs_counter, notification);
}


/******************************************************************************/
/*** FRIDGE PROCESS Resources
 *
 *
 *
 */
//MEAN
PERIODIC_RESOURCE(frige_process_mean, METHOD_GET, COAP_URI_FP_MEAN , "title=\"the average of power (P)\";rt=\"ucum.Watt\";ct=\"0\";if=\"core.s\";obs", 3*CLOCK_SECOND );// specify the information
void
frige_process_mean_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) // GET value
{
  //wdt_disable();
  
  double scaled_value = fridge_get_mean();
  
  snprintf(temp , sizeof(temp), "%0.7f", scaled_value);
  
  REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

void
frige_process_mean_periodic_handler(resource_t *r) // OBSERVE value
{
  static uint16_t obs_counter = 0;
  ++obs_counter;

  double scaled_value = fridge_get_mean();
  if(scaled_value<0) scaled_value = 0; // Quick FIX to show only positive values.
  snprintf(temp , sizeof(temp), "%0.7f", scaled_value);

  /* Build notification. */
  coap_packet_t notification[1]; /* This way the packet can be treated as pointer as usual. */
  coap_init_message(notification, COAP_TYPE_NON, REST.status.OK, 0 );
  coap_set_payload(notification, (uint8_t*)temp , strlen(temp));

  /* Notify the registered observers with the given message type, observe option, and payload. */
  REST.notify_subscribers(r, obs_counter, notification);
}

//UP_TIME
PERIODIC_RESOURCE(frige_process_up_time, METHOD_GET, COAP_URI_FP_UP_TIME, "title=\"the time of power_ON (P)\";rt=\"ucum.Sec\";ct=\"0\";if=\"core.s\";obs", 3*CLOCK_SECOND );// specify the information
void
frige_process_up_time_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) // GET value
{
 
  unsigned long scaled_value = fridge_get_up_time();
  if(scaled_value<0) scaled_value = 0; // Quick FIX to show only positive values.
  snprintf(temp , sizeof(temp), "%lu", scaled_value);
  REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

void
frige_process_up_time_periodic_handler(resource_t *r) // OBSERVE value
{
  static uint16_t obs_counter = 0;
  ++obs_counter;

  unsigned long scaled_value = fridge_get_up_time();
  if(scaled_value<0) scaled_value = 0; // Quick FIX to show only positive values.
  snprintf(temp , sizeof(temp), "%lu", scaled_value);

  /* Build notification. */
  coap_packet_t notification[1]; /* This way the packet can be treated as pointer as usual. */
  coap_init_message(notification, COAP_TYPE_NON, REST.status.OK, 0 );
  coap_set_payload(notification, (uint8_t*)temp , strlen(temp));

  /* Notify the registered observers with the given message type, observe option, and payload. */
  REST.notify_subscribers(r, obs_counter, notification);
}

//TOTAL_TIME
PERIODIC_RESOURCE(frige_process_total_time, METHOD_GET, COAP_URI_FP_TOTAL_TIME, "title=\"the total time of power (P)\";rt=\"ucum.Sec\";ct=\"0\";if=\"core.s\";obs", 3*CLOCK_SECOND );// specify the information
void
frige_process_total_time_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset) // GET value
{
  //wdt_disable();
 
  unsigned long scaled_value = fridge_get_total_time();
  snprintf(temp , sizeof(temp), "%lu", scaled_value);
  REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

void
frige_process_total_time_periodic_handler(resource_t *r) // OBSERVE value
{
  static uint16_t obs_counter = 0;
  ++obs_counter;
  unsigned long scaled_value = fridge_get_total_time();
  snprintf(temp , sizeof(temp), "%lu", scaled_value);

  /* Build notification. */
  coap_packet_t notification[1]; /* This way the packet can be treated as pointer as usual. */
  coap_init_message(notification, COAP_TYPE_NON, REST.status.OK, 0 );
  coap_set_payload(notification, (uint8_t*)temp , strlen(temp));

  /* Notify the registered observers with the given message type, observe option, and payload. */
  REST.notify_subscribers(r, obs_counter, notification);
}
// END FRIDGE_PROCESS

/******************************************************************************/
/******************************************************************************/

/******************************************************************************/
/*** Temperature
 *
 */

RESOURCE(maxim_temperature, METHOD_GET, "temp", "title=\"MAXIM 78M6610 Chip Temperature\";rt=\"ucum.Cel\";ct=\"0\";if=\"core.s\"");
void
maxim_temperature_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  double scaled_value = (double)maxim_78M6610_get_temperature_as_signed_int() / (double)maxim_78M6610_get_register_uint24(MAXIM_78M6610_T_SCALE);
  snprintf(temp , sizeof(temp) , "%0.3f", scaled_value);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


/******************************************************************************/
/******************************************************************************/
/***  RELAY Control
 */

uint8_t relay_logical_state_set = 0; // Relay process start on OFF State

RESOURCE(relay,  METHOD_GET | METHOD_PUT | METHOD_POST , COAP_URI_POWER "/rel", "title=\"Homadeus Load Relay\";rt=\"ipso.pwr.rel\";ct=\"0\";if=\"core.a\"");
void
relay_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{

	//coap_packet_t *const coap_req = (coap_packet_t *) request;
	uint8_t method = REST.get_method_type(request);

	if (method & METHOD_GET)
	{

		//Implement Method GET
		char message[3];
		int length = sprintf(message, "%u", relay_logical_state_set );
		memcpy(buffer, message , length);

		REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
		REST.set_response_payload(response, buffer, length);
		return;
	}else if (method & METHOD_POST)
	{
		// POST STATE to Homadeus Relay. Post state on a core.Actuator Inverts the current State (ON OFF)
		relay_logical_state_set = !relay_logical_state_set;
		process_post(&HOMADEUS_RELAY_PROCESS, HOMADEUS_RELAY_EVENT, &relay_logical_state_set);
		REST.set_response_status(response, REST.status.CHANGED);

	}else if (method & METHOD_PUT)
	{
		// Implement PUT
	    uint8_t *incoming = NULL;
	    size_t len = 0;

		if((len = REST.get_request_payload(request, (const uint8_t **) &incoming)))
		{ 	//PARSE STATE
			int success = 0;
			static uint8_t relay_put_parsed_state = 1;
			if (len == 1){
				if( incoming[0] == '0'){
					relay_put_parsed_state = 0;
					success = 1;
				}else if (incoming[0] == '1'){
					relay_put_parsed_state = 1;
					success = 1;
				}
			}

		    // Response
		    REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
		    REST.set_response_payload(response, NULL, 0);

			if (!success) {
				REST.set_response_status(response, REST.status.BAD_REQUEST);
				char str[40];
				len = sprintf(str, "len:%d . incoming:%s", len, (char*)incoming);
				REST.set_response_payload(response, str, len);
				return;
			}

			// POST STATE to Homadeus Relay
			relay_logical_state_set = relay_put_parsed_state;
			process_post(&HOMADEUS_RELAY_PROCESS, HOMADEUS_RELAY_EVENT, &relay_logical_state_set);
			REST.set_response_status(response, REST.status.CHANGED);
			return;

		}else
	    {
			REST.set_response_status(response, REST.status.BAD_REQUEST);
			const char *error_msg = "NoPayload";
			REST.set_response_payload(response, error_msg, strlen(error_msg));
			return;
	    }

	}
}


/******************************************************************************/
/******************************************************************************/
/*** ATMEL-AVR Chip Temperature
 *
 */

enum ds7505_registers {
    P_TEMP = 0x0, // temperature
    P_CONF = 0x1, // configuration
    P_THYST = 0x2, // Thyst
    P_TOS = 0x3, // Tos
};

enum ds7505_resolutions {
  RES_09 = 0x0, /*!<  9 bit res */
  RES_10 = 0x1, /*!< 10 bit res */
  RES_11 = 0x2, /*!< 11 bit res */
  RES_12 = 0x3, /*!< 12 bit res */
};


RESOURCE(temperature, METHOD_GET, "avr/sen/temp", "title=\"AVR Internal Temperature Sensor\";rt=\"ucum.Cel\";ct=\"0\";if=\"core.s\"");
void
temperature_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
//void temperature_handler(REQUEST* request, RESPONSE* response)
{
  const uint8_t a0 = 0, a1 = 0, a2 = 0;
  const uint8_t resolution = (RES_12 << 5);
  const uint8_t i2c_addr = 0x48 | (a2 & 0x1) << 2 | (a1 & 0x1) << 1 | (a0 & 0x1);
  uint8_t set_config[] = { P_CONF,  resolution };
  uint8_t get_temp[] = { P_TEMP };
  uint8_t temp[2];

  twi_writeTo(i2c_addr, set_config, sizeof(set_config), 1);
  twi_writeTo(i2c_addr, get_temp, sizeof(get_temp), 1);
  twi_readFrom(i2c_addr, temp, sizeof(temp));

  double s = 1.0, temperature = 0.0;
  if ((temp[0] & 0x80) == 0x80) {
    s = -1.0;
    temp[0] &= 0x7f;
  }
  else {
    s = 1.0;
  }

  temperature = s * 0.5 * ((temp[1] & 0x80 )>> 7) + 0.25 * ((temp[1] & 0x40 )>> 6)+ 0.125 * ((temp[1] & 0x20 )>> 5) + 0.0625 * ((temp[1] & 0x10 )>> 4) + (float) temp[0];

  static char message[10];
  int length = snprintf(message, sizeof(message), "%0.4f", temperature);
  //int length = snprintf((char *)buffer, sizeof(buffer), "%0.4f", temperature);

  memcpy(buffer, message, length);

  REST.set_header_content_type(response, REST.type.TEXT_PLAIN); /* text/plain is the default, hence this option could be omitted. */
  REST.set_header_etag(response, (uint8_t *) &length, 1);
  REST.set_response_payload(response, buffer, length);

}

/******************************************************************************/
/******************************************************************************/
// IPSO DEVICE Resources


#define COAP_URI_DEVICE	"dev"

RESOURCE(device_manufacturer, METHOD_GET, COAP_URI_DEVICE"/mfg", "title=\"Manufacturer\";rt=\"ipso.dev.mfg\";ct=\"0\";if=\"core.rp\"");
void
device_manufacturer_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  snprintf(temp , sizeof(temp) , "%s", "Homadeus");
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


RESOURCE(device_model, METHOD_GET, COAP_URI_DEVICE"/mdl", "title=\"Model\";rt=\"ipso.dev.mdl\";ct=\"0\";if=\"core.rp\"");
void
device_model_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  snprintf(temp , sizeof(temp) , "%s", "Homadeus SmartPlug");
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


RESOURCE(device_name, METHOD_GET, COAP_URI_DEVICE"/n", "title=\"Name\";rt=\"ipso.dev.n\";ct=\"0\";if=\"core.rp\"");
void
device_name_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  snprintf(temp , sizeof(temp) , "%s", "Adam Plug");
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


RESOURCE(device_serial, METHOD_GET, COAP_URI_DEVICE"/ser", "title=\"Serial\";rt=\"ipso.dev.ser\";ct=\"0\";if=\"core.rp\"");
void
device_serial_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  snprintf(temp , sizeof(temp) , "%s", "00000001");
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}

RESOURCE(device_uptime, METHOD_GET, COAP_URI_DEVICE"/uptime", "title=\"Uptime\";rt=\"ipso.dev.uptime\";ct=\"0\";if=\"core.s\"");
void
device_uptime_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  unsigned long uptime = clock_seconds();
  snprintf(temp , sizeof(temp) , "%lu", uptime);
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}


/******************************************************************************/
// IPSO LOCATION Resources


#define COAP_URI_LOCATION	"loc"
RESOURCE(location_semantic, METHOD_GET, COAP_URI_LOCATION"/sem", "title=\"Semantic Location\";rt=\"ipso.loc.sem\";ct=\"0\";if=\"core.s\"");
void
location_semantic_handler(void* request, void* response, uint8_t *buffer, uint16_t preferred_size, int32_t *offset)
{
  snprintf(temp , sizeof(temp) , "%s", "Test Room");
  REST.set_response_payload(response,  (uint8_t*)temp, strlen(temp));
}




PROCESS(rest_server_homadeus, "Homadeus CoAP Server");
AUTOSTART_PROCESSES(&rest_server_homadeus, &HOMADEUS_RELAY_PROCESS , &HOMADEUS_FRIDGE_PROCESS);// call the protothread of function created

PROCESS_THREAD(rest_server_homadeus, ev, data)
{
  PROCESS_BEGIN();

  PRINTF("Starting Homadeus CoAP Server\n");

#ifdef RF_CHANNEL
  PRINTF("RF channel: %u\n", RF_CHANNEL);
#endif
#ifdef IEEE802154_PANID
  PRINTF("PAN ID: 0x%04X\n", IEEE802154_PANID);
#endif

  PRINTF("uIP buffer: %u\n", UIP_BUFSIZE);
  PRINTF("LL header: %u\n", UIP_LLH_LEN);
  PRINTF("IP+UDP header: %u\n", UIP_IPUDPH_LEN);
  PRINTF("REST max chunk: %u\n", REST_MAX_CHUNK_SIZE);

  twi_init();

  /* Initialize the REST engine. */
  rest_init_engine();

  /* Activate the application-specific resources.*/
  rest_activate_resource(&resource_temperature); // AVR Chip Temperature Sensor



  //Relay Resource
  rest_activate_resource(&resource_relay);

  //Frequency
  rest_activate_periodic_resource(&periodic_resource_maxim_frequency);//Observable

  //Temperature
  rest_activate_resource(&resource_maxim_temperature);

  //Harmonics
  rest_activate_resource(&resource_maxim_harmonic_selector);

  //Samplig Rate Registers
  rest_activate_resource(&resource_maxim_accumulation_interval);
  rest_activate_resource(&resource_maxim_divisor);


  //Voltage
  rest_activate_resource(&resource_maxim_voltage);

  rest_activate_resource(&resource_maxim_voltage_fundamental);
  rest_activate_resource(&resource_maxim_voltage_harmonic);
  rest_activate_resource(&resource_maxim_voltage_high);
  rest_activate_resource(&resource_maxim_voltage_low);
  rest_activate_resource(&resource_maxim_voltage_peak);



  //Current
  rest_activate_periodic_resource(&periodic_resource_maxim_current);//Observable

  rest_activate_resource(&resource_maxim_current_fundamental);
  rest_activate_resource(&resource_maxim_current_harmonic);
  rest_activate_resource(&resource_maxim_current_high);
  rest_activate_resource(&resource_maxim_current_low);
  rest_activate_resource(&resource_maxim_current_peak);

  //Power
  rest_activate_periodic_resource(&periodic_resource_maxim_power); //Observable
  rest_activate_resource(&resource_maxim_power_avg);

  rest_activate_resource(&resource_maxim_power_apparent);
  rest_activate_resource(&resource_maxim_power_reactive);

  rest_activate_resource(&resource_maxim_power_factor);

  rest_activate_resource(&resource_maxim_power_fundamental_active);
  rest_activate_resource(&resource_maxim_power_fundamental_reactive);
  rest_activate_resource(&resource_maxim_power_fundamental_apparent);

  rest_activate_resource(&resource_maxim_power_harmonic_active);
  rest_activate_resource(&resource_maxim_power_harmonic_reactive);
  rest_activate_resource(&resource_maxim_power_harmonic_apparent);


  // IPSO Device Resources
  rest_activate_resource(&resource_device_manufacturer);
  rest_activate_resource(&resource_device_model);
  rest_activate_resource(&resource_device_name);
  rest_activate_resource(&resource_device_serial);
  rest_activate_resource(&resource_device_uptime);

  rest_activate_resource(&resource_location_semantic);



  // TB Maxim DEBUG Registers
  rest_activate_resource(&resource_maxim_sensors);
  rest_activate_resource(&resource_maxim_scales);
  rest_activate_resource(&resource_maxim_sensors_unscaled);

  // Fridge Process
  rest_activate_periodic_resource(&periodic_resource_frige_process_mean);		//MEAN
  rest_activate_periodic_resource(&periodic_resource_frige_process_up_time);	//UP_TIME
  rest_activate_periodic_resource(&periodic_resource_frige_process_total_time);	//TOTAL_TIME 

  /* Define application-specific events here. */
  while(1) {
    PROCESS_WAIT_EVENT();
#if defined (PLATFORM_HAS_BUTTON)
    if (ev == sensors_event && data == &button_sensor) {
      PRINTF("BUTTON\n");

#if REST_RES_EVENT
      /* Call the event_handler for this application-specific event. */
      event_event_handler(&resource_event);
#endif
#if REST_RES_SEPARATE
      /* Also call the separate response example handler. */
      separate_finalize_handler();
#endif


    }
#endif /* PLATFORM_HAS_BUTTON */
  } /* while (1) */

  PROCESS_END();
}
